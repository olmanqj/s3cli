# s3cli

A Command Line Interface (CLI) for interacting with s3 nodes.


## Workflow

1. Load the Space System Model of your target node.

Use the `spawn` command or pass the command to s3cli directly.
~~~
s3cli "spawn example.yaml"
~~~





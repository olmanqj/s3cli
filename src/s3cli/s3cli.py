#!/usr/bin/env python
"""A simple cmd2 application."""
import cmd2
#from s3py import * 
import s3py as s3
from s3transpiler import S3Transpiler


banner = """
     |___ \\     | (_)
  ___  __) | ___| |_ 
 / __||__ < / __| | |
 \\__ \\___) | (__| | |
 |___/____/ \\___|_|_|
"""

_s3_sys = s3.Sys(None, "s3")
_s3_cli = s3.Sys(_s3_sys, "cli")
_s3_pubs = s3.Sys(_s3_sys, "pubs")
_s3_subs = s3.Sys(_s3_sys, "subs")


import socket
from threading import Thread
from time import sleep



class S3BaseApp(s3.Sys):
    def __init__(self, name:str):
        super().__init__(_s3_pubs, name)
        self.enabled     = s3.ParamBool(self, "ena",  False)
        self.error       = s3.ParamU8(self,   "err",  0)

        self.enabled.reg_callback(self.toggle)
        self.thread = Thread(target=self.loop)
        self.thread.daemon = True
        self.thread.start()

    def enable(self):
        self.error.set(0)
        self.enabled.set(True)

    def disable(self):
        self.enabled.set(False)
        
    def toggle(self):
        if self.enabled.get == True:
            self.disable()
        else:
            self.enable()

    def on_error(self, code):
        self.error.set(code)
        self.disable()


class S3UdpPublisher(s3.Sys):

 
    def __init__(self, name:str, group_name:str, dest_adrr: str, dest_port:int):
        super().__init__(_s3_pubs, name)
        self.enabled     = s3.ParamBool(self, "ena",  False)
        self.error       = s3.ParamU8(self,   "err",  0)
        self.group_name  = s3.ParamStr(self,  "grp",  16, group_name)
        self.link        = s3.ParamStr(self,  "link", 16, "UDP")
        self.port        = s3.ParamF32(self,  "port", dest_port)
        self.address     = s3.ParamStr(self,  "addr", 16, dest_adrr)
        self.frequency   = s3.ParamF32(self,  "freq", 0.5)
        self.tx_count    = s3.ParamU32(self,  "tx", 0)

        self.soc = None
        self.group = None

        self.enabled.reg_callback(self.toggle)
        
        self.thread = Thread(target=self.loop)
        self.thread.daemon = True
        self.thread.start()


    def enable(self):
        self.enabled.set(True)

    def disable(self):
        self.enabled.set(False)
        if self.soc:
            self.soc.close()

    def toggle():
        pass


    def on_error(self, code):
        self.error.set(code)
        self.disable()


    def loop(self):
        while True:

            if self.enabled.get() and not self.soc:
                self.error.set(0)
                try:
                    self.soc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                except:
                    self.on_error(1)
                try:
                    self.group = s3.ParamDB().get_group(self.group_name.get())
                except:
                    self.on_error(2)
            elif not self.enabled.get() and self.soc:
                self.soc.close()

            if not self.enabled.get():
                sleep(1)
                continue

            
            sleep(1/self.frequency.get())

            packet = 0
            try:
                packet = self.group.encode()
            except Exception as e:
                self.on_error(3)
                raise e
                continue

            try:        
                self.soc.sendto(packet, (self.address.get(), self.port.get()))
                self.tx_count.set(self.tx_count.get()+1)
            except:
                self.on_error(4)


class S3UdpSubscriber(s3.Sys):

    def __init__(self, name:str, group_name:str, bind_port:int):
        super().__init__(_s3_subs, name)
        self.enabled     = s3.ParamBool(self, "ena",  False)
        self.error       = s3.ParamU8(self,   "err",  0)
        self.group_name  = s3.ParamStr(self,  "grp",  16, group_name)
        self.link        = s3.ParamStr(self,  "link", 16, "UDP")
        self.port        = s3.ParamF32(self,  "port", bind_port)
        #self.address     = s3.ParamStr(self,  "addr", 16, "127.0.0.1")
        self.rx_count    = s3.ParamU32(self,  "rx", 0)

        self.soc = None
        self.group = None

        self.thread = Thread(target=self.loop)
        self.thread.daemon = True
        self.thread.start()


    def on_error(self, code):
        self.enabled.set(False)
        self.error.set(code)
        if self.soc:
            self.soc.close()
            

    def loop(self):


        while True:

            if self.enabled.get() and not self.soc:
                self.error.set(0)
                try:
                    self.soc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                    #self.soc.bind((self.address.get(), self.port.get()))
                    self.soc.bind(("localhost", self.port.get()))
                except:
                    self.on_error(1)
                try:
                    self.group = s3.ParamDB().get_group(self.group_name.get())
                except:
                    self.on_error(2)
            elif not self.enabled.get() and self.soc:
                self.soc.close()

            if not self.enabled.get():
                sleep(1)
                continue
            
            data = 0
            try:
                data, server = self.soc.recvfrom(1024)
                self.rx_count.set(self.rx_count.get()+1)
            except:
                self.on_error(3)
                continue

            try:
                self.group.decode(data)
            except:
                self.on_error(4)               

    


S3UdpPublisher("p1", "conf", "127.0.0.1", 8888)
S3UdpSubscriber("s1", "conf", 8888)



class S3Cli(cmd2.Cmd):
    """s3 CLI."""

    param_db = s3.ParamDB()
    ctx_sys = s3._root

    def __init__(self):
        super().__init__()

        self.prompt = '/> '
        self.debug = True
        delattr(cmd2.Cmd, 'do_edit')
        delattr(cmd2.Cmd, 'do_alias')
        delattr(cmd2.Cmd, 'do_shortcuts')
        delattr(cmd2.Cmd, 'do_macro')


    print_tree_args = cmd2.Cmd2ArgumentParser()
    print_tree_args.add_argument('-i', '--index', action='store_true', help='Show indexes')
    print_tree_args.add_argument('-t', '--type', action='store_true', help='Show type names')
    print_tree_args.add_argument('-s', '--size', action='store_true', help='Show sizes')
    print_tree_args.add_argument('-a', '--all', action='store_true', help='Show everything')
    @cmd2.with_argparser(print_tree_args)
    def do_tree(self, args):
        """Print the Param tree."""
        if args.all:
            args.index = True 
            args.type = True 
            args.size = True
        self.param_db.print_tree(args.index, args.type, args.size)


    get_param_args = cmd2.Cmd2ArgumentParser()
    get_param_args.add_argument('param', type=str, help='Parameter path')
    @cmd2.with_argparser(get_param_args)
    def do_get(self, args):
        """Get the value of parameter."""
        param = self.param_db.search_param(args.param.replace(".", ":"), self.ctx_sys)
        print(param)


    set_param_args = cmd2.Cmd2ArgumentParser()
    set_param_args.add_argument('param', type=str, help='Parameter path')
    set_param_args.add_argument('value', type=str, help='Value to set to path')
    @cmd2.with_argparser(set_param_args)
    def do_set(self, args):
        """Set the value of parameter."""
        param = self.param_db.search_param(args.param.replace(".", ":"), self.ctx_sys)
        param.from_chars(args.value)

    print_ctx_sys_args = cmd2.Cmd2ArgumentParser()
    print_ctx_sys_args.add_argument('-i', '--index', action='store_true', help='Show indexes')
    print_ctx_sys_args.add_argument('-t', '--type', action='store_true', help='Show type names')
    print_ctx_sys_args.add_argument('-s', '--size', action='store_true', help='Show sizes')
    print_ctx_sys_args.add_argument('-a', '--all', action='store_true', help='Show everything')
    @cmd2.with_argparser(print_ctx_sys_args)
    def do_s(self, args):
        """List System content."""
        if args.all:
            args.index = True 
            args.type = True 
            args.size = True
        self.ctx_sys.pprint(index=args.index, type=args.type, size=args.size, max_depth=0)



    change_sys_args = cmd2.Cmd2ArgumentParser()
    change_sys_args.add_argument('sys', choices_provider=lambda cli: [sys.name for sys in cli.ctx_sys._subsys], type=str, help='Sys path')
    @cmd2.with_argparser(change_sys_args)
    def do_cs(self, args):
        """Change the system in context."""
        if args.sys == ".":
            pass
        elif args.sys == "..":
            if self.ctx_sys.parent != None:
                self.ctx_sys = self.ctx_sys.parent 
        elif args.sys == "/":
                self.ctx_sys = s3._root
        else:
            try:
                self.ctx_sys = self.param_db.search_sys(s3.NamePath(args.sys), self.ctx_sys)
            except s3.LibS3Error:
                print(f"Sys '{args.sys}' not found!")
        self.prompt = f'{self.ctx_sys.get_path()}> '




    spawn_args = cmd2.Cmd2ArgumentParser()
    spawn_args.add_argument('file', type=str, help='s3 model file [.yaml or .json]')
    @cmd2.with_argparser(spawn_args)
    def do_spawn(self, args):
        """ Spawn params and systems from s3 Model file."""
        s3trans = S3Transpiler(args.file)
        self.param_db.spawn_from_dict(s3trans.instance)


    groups_args = cmd2.Cmd2ArgumentParser()
    groups_args.add_argument('group', nargs="?", default=None, type=str, help='Show params in a group')
    @cmd2.with_argparser(groups_args)
    def do_group(self, args):
        """Show groups information."""
        if args.group:
            group = self.param_db.get_group(args.group)
            group.pprint()
        else:
            self.param_db.pprint_groups()



if __name__ == '__main__':
    import sys
    print(f"Welcome to {banner}\nType 'help' to list commands.")
    cli = S3Cli()
    sys.exit(cli.cmdloop())